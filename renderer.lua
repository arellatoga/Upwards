renderer = {
	sprites = {},
	thread = nil
}

function renderer:initialize()
	--self.thread = love.thread.newThread('renderer.lua')
	for i = 1, 12 do
		self.sprites[i] = {}
	end
end

--[[
	Six Layers:
	1 - foreground1
	2 - foreground2
	3 - player particles
	4 - player
	5 - enemy particles
	6 - enemies
	7 - interactable particles
	8 - interactables
	9 - static object particles
	10 - static object particles
	11 - background 1
	12 - background 2
--]]

sprite = {
	name = nil,
	id = nil,
	img = nil,
	x = nil,
	y = nil
}

--[[
	addSprite:
	adds sprite to render list
--]]
function renderer:add(name, layer, img, x, y)
	local spr = sprite
	spr.name = name
	spr.img = img
	spr.x = x
	spr.y = y
	table.insert(self.sprites[layer], spr)
end

--[[
	updateSprite:
	updates existing sprite
--]]
function renderer:update(name, layer, img, x, y)
	local spr = sprite
	spr.img = img
	spr.x = x
	spr.y = y
	for i, sprite in ipairs(self.sprites[layer]) do
		if sprite.name == name then
			self.sprites[layer][i] = spr
		end
	end
end

--[[
	removeSprite:
	removes sprite from renderer
--]]
function renderer:remove(name)
	for i = 1, 12 do
		for j, spr in ipairs(self.sprites[i]) do
			if spr.name == name then
				table.remove(self.sprites[i][j])
			end
		end
	end
end

--[[
	renderSprites:
	renders Sprites based on layer
--]]
function renderer:render()
	for i = 1, 12 do
		for j, spr in ipairs(self.sprites[i]) do
			self:draw(spr)
			print (i+j)
		end
	end
end

--[[
	drawSprite
	generic 'draw a sprite'
--]]
function renderer:draw(spr)
	love.graphics.draw(spr.img, spr.x, spr.y)
end
