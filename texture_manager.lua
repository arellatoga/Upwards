texture_manager = {
	textures = {},
	thread = nil
}

function texture_manager:initialize()
	thread= love.thread.newThread('texture_manager.lua')
end

--[[
	load_texture:
	stores textures in texture array
--]]
function texture_manager:load_texture(name, path)
	self.textures[name] = love.graphics.newImage(path)
end

--[[
	get_texture:
	get texture specified by name
--]]
function texture_manager:get_texture(name)
	if self.textures[name] == nil then
		print("Texture not found")
		return nil
	else
		return self.textures[name]
	end
end
