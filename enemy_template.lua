-- Enemy Template
-- Use this for uhm making enemies lol

local enemyFilter = function(item, other)
	if other.isPlayer then
		return 'cross'
	elseif other.isWeapon then
		return 'cross'
	elseif other.isWall then
		return 'slide'
	end
end

test_obj2 = {
	img = nil,
	tag = "enemy",
	x = 400,
	y = 100,
	health = 500,
	isAlive = true,
	isEnemy = true,
	bounce_x = 0,
	bounce_y = 0,
	dir = 0,
	x_velocity = 100,
	y_velocity = 100
}

function test_obj2:load()
	texman:load_texture("enemy1", "suiyoubi.png")
	self.img = texman:get_texture("enemy1")
	world:add(self, self.x, self.y, 64, 64)
	self.x_velocity = 0
end

function test_obj2:compare(x, y)
	if self.x > x then
		return 1
	else
		return 0
	end
end

function test_obj2:draw(dt)
	love.graphics.draw(self.img, self.x, self.y)
end

function test_obj2:update(dt)
	local goal_x = self.x + (self.x_velocity + self.bounce_x) * dt
	local goal_y = self.y + (self.y_velocity + self.bounce_y) * dt

	self.bounce_x = self.bounce_x * 0.9
	self.bounce_y = self.bounce_y * 0.9

	if self.bounce_x < 0.0001 then
		self.bounce_x = 0
	end

	if self.bounce_y < 0.0001 then
		self.bounce_y = 0
	end

	local actual_x, actual_y, cols, len = world:move(self, goal_x, goal_y, enemyFilter)

	for i = 1, len do
		if cols[i].other.isWeapon then
			local type = 0
			if cols[i].other.att_a > 0.002 then
				type = 1
			elseif cols[i].other.att_b > 0.002 then
				type = 2
			elseif cols[i].other.att_c > 0.002 then
				type = 3
			end
			self:onWeaponHit(type, cols[i].other.dir)
		end
	end

	self.x = actual_x
	self.y = actual_y

	if actual_y == goal_y then
		--self.bounce_y = 0
	end

	if self.y_velocity < 500 then
		self.y_velocity = self.y_velocity + 1000 * dt
	end

	-- Destroy self if dead
	if self.health <= 0 then
		self.isAlive = false
		world:remove(self)
	end
end

function test_obj2:onWeaponHit(type, dir)
	self.health = self.health - 10
	print ("I got hit!")
	if type == 2 then
		self.y_velocity = -500
	elseif type == 3 then
		self.bounce_y = 1500
	elseif type == 1 then
		print (dir)
		if dir == 2 then
			self.bounce_x = -400 + (self.x_velocity)
		elseif dir == 1 then
			self.bounce_x = 400 + (-self.x_velocity)
		end
	end
end