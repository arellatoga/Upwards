require ('camera')
require ('player')
require ('physics')
require ('texture_manager')
require ('enemy_template')
bump = require 'bump'

texman = texture_manager
p = player

world = bump.newWorld(50)

bg = nil

constants = {
	gravity = 100
}

test_obj = {
	img = nil,
	tag = "wall",
	x = 100,
	y = 600,
	isWall = true
}

renderlayers = {}

function love.load()
	texman:initialize()
	
	-- Initialize render layers
	renderlayers["bg1"] = {}
	renderlayers["bg2"] = {}
	renderlayers["p"] = {}
	renderlayers["pfx"] = {}
	renderlayers["e"] = {}
	renderlayers["efx"] = {}
	renderlayers["npc"] = {}
	renderlayers["npcfx"] = {}
	renderlayers["o"] = {}
	renderlayers["ofx"] = {}
	renderlayers["fg"] = {}

	p.load()
	test_obj2:load()

	test_obj.img = love.graphics.newImage('ground.png')
	bg = love.graphics.newImage('brick.png')

	world:add(p, p.x, p.y, p.img:getWidth(), p.img:getHeight())
	world:add(test_obj, test_obj.x, test_obj.y, 2000, 100)
end

function love.update(dt)
	p.update(dt)

	if test_obj2.isAlive then
		test_obj2:update(dt)
	end

	if not p.isFlying then
		camera.follow(p.camera_x, p.camera_y, p.direction, dt)
	else
		camera.follow2(p.camera_x, p.camera_y, p.direction, dt)
	end
end

function love.draw(dt)
	camera.set()

	love.graphics.setColor(255, 255, 255, 255)
	love.graphics.draw(bg, 0 - bg:getWidth()/2, 0 - bg:getHeight()/2)

	for i = -25, 25  do
		for j = -25, 25 do
			love.graphics.draw(bg, i*64, j*64)
		end
	end

	p:draw(dt)
	if test_obj2.isAlive then
		test_obj2:draw(dt)
	end
	love.graphics.setColor(0, 0, 255, 255)
	love.graphics.rectangle("fill", test_obj.x, test_obj.y, 2000, 100)

	love.graphics.setColor(255, 255, 255)
	camera.unset()

	-- Draw HUD here!
	drawGUI()
end

function drawGUI()
	love.graphics.setColor(255, 100, 100, 255)
	love.graphics.rectangle("fill", 100, 100, (player.flyTime/player.flyTimeMax) * 300, 14)
	love.graphics.setColor(255, 255, 255)
	love.graphics.print(" Fly Meter", 100, 100)

	love.graphics.setColor(255, 255, 255)

	local stats = love.graphics.getStats()
	local str = string.format("VRAM: %.2f MB", stats.texturememory / 1024 / 1024)
	local str2 = string.format("Drawcalls: %.1f", stats.drawcalls)
	local str3 = string.format('RAM (kB) %.1f ', collectgarbage('count'))

	love.graphics.print(str, 25, 35)
	love.graphics.print(love.timer.getFPS(), 25 ,25)
	love.graphics.print(str2, 25, 45)
	love.graphics.print(str3, 24, 55)
end

function addToLayer(img, layer)
	if renderlayers[layer] == nil then
		return
	else
		table.insert(renderlayers[layer], img)
	end
end
