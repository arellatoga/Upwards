function love.conf(t)
	t.title = "Upwards!"
	t.window.width = 1024
	t.window.height = 600
	t.console = true 
	t.window.fullscreen = false
	t.window.vsync = true
	t.window.msaa = 1
end