img = nil
imgX = 250

camera = {
	x = 0,
	y = 0,
	scaleX = 1,
	scaleY = 1,
	rotation = 0
}

function camera:set()
	love.graphics.push()
	love.graphics.rotate(-self.rotation)
	love.graphics.scale(1 / self.scaleX, 1 / self.scaleY)
	love.graphics.translate(-self.x, -self.y)
end

function camera:unset()
	love.graphics.pop()
end

function camera:move(dx, dy)
	self.x = self.x + (dx or 0)
	self.y = self.y + (dy or 0)
end

function camera:rotate(dr)
	self.rotation = self.rotation + dr
end

function camera:scale(sx, sy)
	sx = sx or 1
	self.scaleX = self.scaleX * sx
	self.scaleY = self.scaleY * (sy or sx)
end

function camera:setPosition(x, y)
	self.x = x or self.x
	self.y = y or self.y
end

function camera:setScale(sx, sy)
	self.scaleX = sx or self.scaleX
	self.scaleY = sy or self.scaleY
end

function love.load()
	img = love.graphics.newImage("assets/plane.png")
end

function love.update(dt)
	if love.keyboard.isDown('a') then
		imgX = imgX - (150 * dt)
	elseif love.keyboard.isDown('d') then
		imgX = imgX + (150 * dt)
	end
	
	if love.keyboard.isDown('q') then
		camera.x = camera.x - (150 * dt)
	elseif love.keyboard.isDown('e') then
		camera.x = camera.x + (150 * dt)
	end
end

function love.draw(dt)
	camera:set()
	love.graphics.draw(img, imgX, 250, 0, 1, 1, img:getWidth() / 2, img:getHeight() / 2)
	camera:unset()
end


