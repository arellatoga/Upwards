require ('weapon_filter')

att_blaster = {
	tag = "blast",
	damage = 100,
	img = NULL,
	x = 0,
	y = 0,
	dir = 0,
	att_a = 0,
	att_b = 0,
	att_c = 0,
	do_att_a = false,
	do_att_b = false,
	do_att_c = false,
	cooldown = 0,
	cooldown_current = 0,
	is_attacking = false,
	isWeapon = true,
	has_collider = false
}

-- Load and store images and stuff
function att_blaster:load()
	texman:load_texture("def_a", "te.png")
	texman:load_texture("def_b", "ue.png")
	texman:load_texture("def_c", "shita.png")
end

-- Ujpdate function, gets called every frame
function att_blaster:update(x, y, dir, dt)
	if self.att_a > 0.002 or self.att_b > 0.002 or self.att_c > 0.002 then -- Attack frames
		if self.att_a > 0.002 then
			self.att_a = self.att_a - dt
			self.do_att_a = true
		elseif self.att_b > 0.002 then
			self.att_b = self.att_b - dt
			self.do_att_b = true
		elseif self.att_c > 0.002 then
			self.att_c = self.att_c - dt
			self.do_att_c = true
		end

		self.x = x
		self.y = y
		self.dir = dir
		local offset = 0

		if self.dir == 2 then
			offset = -p.img:getWidth() - self.img:getWidth()
		elseif self.dir == 1 then
			offset = 0
		end

		self.x = self.x + offset
		world:update(self, self.x, self.y, 64, 64, weaponFilter)
	else -- Remove attack colliders from physics world then apply cooldown
		if self.is_attacking and self.cooldown == self.cooldown_current then
			world:remove(self)
			self.has_collider = false
			self.do_att_a = false
			self.do_att_b = false
			self.do_att_c = false
		end

		if self.cooldown > 0.002 then
			self.cooldown = self.cooldown - dt
		else
			self.is_attacking = false
		end
	end
end

-- Draw whatever this thing is
function att_blaster:draw(x, y, dir)
	local offx = 0

	if self.dir == 1 then
		offx = self.img:getWidth()
	elseif self.dir == 2 then
		offx = -self.img:getWidth()
	end

	if self.do_att_a or self.do_att_b or self.do_att_c then
		love.graphics.draw(self.img, self.x, self.y)
	end
end

-- Do first attack
-- clean up functions attack_one and attack_two, merge them later

function att_blaster:attack(type)
	if type == 1 then
		self.img = texman:get_texture("def_a")
		self.att_a = 0.1
		self.att_b = 0.0
		self.att_c = 0.0
	elseif type == 2 then
		self.img = texman:get_texture("def_b")
		self.att_a = 0.0
		self.att_b = 0.3
		self.att_c = 0
	elseif type == 3 then
		self.img = texman:get_texture("def_c")
		self.att_a = 0
		self.att_b = 0
		self.att_c = 0.3
	end

	self.cooldown = 0.05
	self.cooldown_current = 0.05
	local offset = 0

	if self.dir == 2 then
		offset = -10
	elseif self.dir == 1 then
		offset = 10
	end

	if self.has_collider == false then
		world:add(self, 0, 0, self.img:getWidth(), self.img:getHeight())
	end
	world:update(self, self.x + offset, self.y, 64, 64, weaponFilter)

	self.is_attacking = true
	self.has_collider = true
end

function att_blaster:isAttacking()
	if self.is_attacking then
		return true
	else
		return false
	end
end