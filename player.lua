require ('attack_default')

text = {
	text = "",
	x = 0,
	y = 0
}

player = {
	-- !IMPORTANT
	tag = "player",
	camera_x = 0, -- Camera controls, ignore
	camera_y = 0, -- Camera controls, ignore
	health = 100,
	--! x and y coordinates denote UPPER LEFT CORNER !!!
	x = 450, -- Current x-coordinate position
	y = 300, -- Current y-coordinate position
	speed = 500, -- Maximum movement speed
	x_velocity = 0, -- Current velocity
	y_velocity = 0, -- Current velocity
	img = nil, -- Image file. convert to quads later
	direction = 0, -- Current direction faced
	-- 0 - left, 1 - right, 2 - down, 3 - up
	isGrounded = false,
	isPlayer = true,
	isFlying = false,
	flyTime = 0,
	flyTimeMax = 3,
	current_Wep = nil
}

-- Filter for physical collisions
local playerFilter = function(item, other)
	if other.isWall then
		return 'slide'
	elseif other.isEnemy then
		return 'cross'
	elseif other.isNPC then
		return 'cross'
	elseif other.isExit then
		return 'touch'
	end
end

function player.load()
	texman:load_texture("player", "wtf.png")
	player.img = texman:get_texture("player")
	player.camera_x = player.img:getWidth()
	player.y_velocity = player.speed
	flyText = text
	player.current_wep = att_blaster
	player.current_wep:load()

end

function player.update(dt)
	flyText.text = player.flyTime
	flyText.x = player.x - 200
	flyText.y = player.y - 200

	if player.flyTime <= 0 then
		player.isFlying = false
		player.flyTime = 0
	end

	if not player.isFlying then
		local items, len = world:querySegment(player.x + 1, player.y + player.img:getHeight() - 1,
				player.x + player.img:getWidth() - 1, player.y + player.img:getHeight() + 1, filter)
		if len > 1 and items[len].tag == "wall" then
			player.isGrounded = true
			if player.flyTime < player.flyTimeMax then
				player.flyTime = player.flyTime + dt*2
			end
		else
			player.isGrounded = false
		end
	else
		player.flyTime = player.flyTime - dt/2
	end

	player.ground_movement(dt)
	player.move(dt)

	if player.camera_y ~= player.y - 300 then
		player.camera_y = player.y - 300 + (player.img:getHeight() / 2)
	end

	player.current_wep:update(player.x + 64, player.y, player.direction, dt)
end

function player:draw(dt)
	love.graphics.draw(self.img, self.x, self.y)
	player.current_wep:draw(player.x, player.y, player.direction)
end

function player.ground_movement(dt)
	-- Gravity stuff
	-- Basically if its falling speed is less than the terminal speed
	-- (that's player.speed), it keeps falling
	if not player.isFlying then
		-- Left-Right movement
		local drag = 1
		if love.keyboard.isDown('d') then
			player.x_velocity = player.speed * drag
			player.direction = 1
		elseif love.keyboard.isDown('a') then
			player.x_velocity = -player.speed * drag
			player.direction = 2
		else
			player.x_velocity = player.x_velocity - (player.x_velocity * 0.75)
		end

		if player.y_velocity <= player.speed * 3 then
			player.y_velocity = player.y_velocity + 1000 * dt
		end
	else --if isFlying
		local dir = 0
		if love.keyboard.isDown('d') then
			player.x_velocity = player.speed * 2
			dir = dir + 1
		elseif love.keyboard.isDown('a') then
			player.x_velocity = -player.speed * 2
			dir = dir + 2
		else
			player.x_velocity = 0
		end

		if love.keyboard.isDown('w') then
			player.y_velocity = -player.speed * 2
			dir = dir + 8
		elseif love.keyboard.isDown('s') then
			player.y_velocity = player.speed * 2
			dir = dir + 4
		else
			player.y_velocity = 0
		end

		player.direction = dir
	end
end

function love.keypressed(key)
	if key == 'e' then
		if player.isFlying then
			player.isFlying = false
		else
			player.isFlying = true
		end
		print ("Switched movement modes")
	end

	if key == "j" and player.current_wep.att_a < 0.002 then
		player.attack(1)
	elseif key == "k" and player.current_wep.att_b < 0.002 then
		player.attack(2)
	elseif key == "l" and player.current_wep.att_c < 0.002 then
		player.attack(3)
	end

	if not player.isFlying then
		if key == "w" and player.isGrounded then
			player.y_velocity = -player.speed
		end
	end
end

function player.move(dt)
	local goal_x = player.x + (player.x_velocity * dt)
	local goal_y = player.y + (player.y_velocity * dt)

	local actual_x, actual_y, cols, len = world:move(p, goal_x, goal_y, playerFilter)

	player.x = actual_x
	player.y = actual_y

	if goal_x == actual_x then
		-- player.camera_x = player.camera_x + (player.x_velocity * dt)
		player.camera_x = actual_x - (love.graphics.getWidth()/2 + 32) + (player.img:getWidth())
	end
	if goal_y == actual_y then
		-- player.camera_y = player.camera_y + (player.y_velocity * dt)
		player.camera_y = actual_y - (love.graphics.getHeight()/2) - (player.img:getHeight())
		player.on_ground = true
	else
		player.on_ground = false
	end
end

function player.attack(att)
	print("Pressed attack button")
	if att == 1 then
		player.current_wep:attack(1)
	elseif att == 2 then
		player.current_wep:attack(2)
	elseif att == 3 then
		player.current_wep:attack(3)
	end
end

--[[
FOUR BITS
1 1 1 1
UP DOWN LEFT RIGHT
--]]
function player:compare(x, y)
	local dir = 0

	if self.x > x then
		dir = dir + 1
	elseif self.x < x then
		dir = dir + 2
	end

	if self.y > y then -- below
		dir = dir + 4
	elseif self.y < y then
		dir = dir + 8
	end

	return dir
end
