-- Camera implementation
-- Adapted from nova-fusion.com/2011/04/19/cameras-in-love2d-part-1-the-basics/
-- camera.follow() is mine though

-- Initialize camera module
camera = {
	x = 350,
	y = 0,
	scaleX = 1,
	scaleY = 1,
	movespeed = 150,
	rotation = 0
}

-- Set the camera
-- call this after love.update() starts
function camera.set()
	love.graphics.push()
	love.graphics.rotate(-camera.rotation)
	love.graphics.scale(1/camera.scaleX, 1/camera.scaleY)
	love.graphics.translate(-camera.x, -camera.y)
end

-- Unset the camera
-- call this before love.update() ends
function camera.unset()
	love.graphics.pop()
end

-- Moves the camera
function camera.move(dx, dy)
	camera.x = camera.x + (dx or 0)
	camera.y = camera.y + (dy or 0)
end

-- Rotates the camera
function camera.rotate(dr)
	camera.rotation = camera.rotation + dr
end

-- Scales the camera to appropriate values
function camera.scale(sx, sy)
	sx = sx or 1
	camera.scaleX = camera.scaleX * sx
	camera.scaleY = camera.scaleY * (sy or sx)
end

-- Sets the position of the camera
function camera.setPosition(x, y)
	camera.x = x or camera.x
	camera.y = y or camera.y
end

-- Sets the scale of the camera
function camera.setScale(sx, sy)
	camera.scaleX = sx or camera.scaleX
	camera.scaleY = sy or camera.scaleY
end

-- Follows the object at the following coordinates
-- using linear interpolation smoothing
function camera.follow(follow_x, follow_y, dir, delta)
	local snapback_x = 3 -- Snapback factor for horizontal camera movement
	local snapback_y = 9 -- Snapback factor for vertical camera movement
	local offset = 0

	-- Offset computation
	if dir == 1 then
		offset = love.graphics.getWidth() / 4 
	elseif dir == 2 then
		offset = -love.graphics.getWidth() / 4 
	else
		offset = 0
	end

	--[[ linear interpolation smoothing:
	coordinate = (distance * factor) + coordinate
	--]]
	camera.x = ((follow_x - camera.x + offset) * delta * snapback_x) + camera.x
	camera.y = ((follow_y - camera.y) * delta * snapback_y) + camera.y
	-- camera.y = follow_y
end

-- Follows the object at the following coordinates
-- using linear interpolation smoothing
function camera.follow2(follow_x, follow_y, dir, delta)
	local offsetx = 0 -- Offset to change the camera's center
	local offsety = 0
	local snapback_x = 5 -- Snapback factor for horizontal camera movement
	local snapback_y = 5 -- Snapback factor for vertical camera movement

	-- Offset computation

	if dir >= 8 then
		offsety = -love.graphics.getHeight() / 2
		dir = dir - 8
	elseif dir >= 4 then
		offsety = love.graphics.getHeight() / 2
		dir = dir - 4
	end

	if dir == 1 then
		offsetx = love.graphics.getWidth() / 2 + 32
	elseif dir == 2 then
		offsetx = -love.graphics.getWidth() / 2
	end

	print (dir)
	camera.x = ((follow_x - camera.x + offsetx) * delta * snapback_x) + camera.x
	camera.y = ((follow_y - camera.y + offsety) * delta * snapback_y) + camera.y
end
